# stdlib
import functools
import io
import pprint
import time
import traceback


def wrap_exception(func):
	""" Decorator that calls self.on_exception(e).

	If an instance method throws an exception, this decorator will catch it
	and pass it to the on_exception method on the same instance. """

	@functools.wraps(func)
	def wrapper(self, *args, **kwargs):
		try:
			return func(self, *args, **kwargs)
		except Exception as e:
			self.on_exception(e)

	return wrapper


class ExceptionHandler:
	def __init__(self):
		self.exception_delay_init = 45
		self.exception_delay = self.exception_delay_init
		self.debug_info = None
		self.last_event = None

	def reset_exc_delay(self):
		self.exception_delay = self.exception_delay_init

	def _exc_delay(self):
		ret = self.exception_delay
		self.exception_delay *= 2
		return ret

	def on_exception(self, e):
		print("Exception caught:", traceback.format_exception_only(type(e), e)[-1].strip())
		print("Type /debug to show more info.")
		moreinfo = io.StringIO()
		print("Last event received before exception:", file=moreinfo)
		pprint.pprint(self.last_event, moreinfo)
		print("Stack trace:", file=moreinfo)

		traceback.print_exception(type(e), e, e.__traceback__, file=moreinfo)
		self.debug_info = moreinfo.getvalue()
		moreinfo.close()

		delay = self._exc_delay()
		print("Waiting {0} seconds before trying again.".format(delay))
		time.sleep(delay)
		print("Let's go!")
