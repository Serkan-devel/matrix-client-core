from distutils.core import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

#TODO: add reference to testing
setup(
    name='Matrix Client Core',
    version='0.0.1',
    author='Matrixcoffee',
    author_email='Matrixcoffee@users.noreply.github.com',
    packages=['matrix-client-core'],
    url='https://gitlab.com/Matrixcoffee/matrix-client-core/',
    license='Apache License, Version 2.0',
    description='a featureful base platform that you can build wafer-thin (<10LOC if you want) Matrix clients and/or bots on top of.',
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=[
        'requests'
    ]
)

